package com.example.bluenightz.senddatatoserver;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.bluenightz.senddatatoserver.manager.CameraManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    //
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1;
    @BindView(R.id.btn_capture_image)
    Button btn_capture_image;


    @BindView(R.id.btn_viewimage)
    Button btn_viewimage;


    @BindView(R.id.btn_setting)
    Button btn_setting;

    Activity activity;
    Context context;
    String status = "";
    String problemMessage = "nomessage";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        init();

        activity = this;
        context = activity.getBaseContext();
    }

    private void init() {
        btn_capture_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("System", "click callFuncA");
                status = "1";
                callFuncA();
            }
        });

        btn_viewimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("System", "click callFuncB");
                status = "0";
                callFuncB();
            }
        });

        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("System", "click callFuncC");
                status = "2";
                callFuncC();
            }
        });

    }

    private void callFuncC(){

    }

    private void callFuncB() {

    }

    private void callFuncA(){
        if( checkPermissionByTag() ) {
            requestCameraPermission();
        }else{
            takephoto();
        }
    }


    private void takephoto(){
        CameraManager.getInstance().dispatchTakePictureIntent(activity, REQUEST_IMAGE_CAPTURE);
        mCurrentPhotoPath = CameraManager.getInstance().getPhotoPath();
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void requestCameraPermission(){
        ActivityCompat.requestPermissions(activity, new String[]{ Manifest.permission.CAMERA }, REQUEST_CODE_ASK_PERMISSIONS );
    }

    private boolean checkPermissionByTag(){

        return (
            ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch( requestCode ){
            case REQUEST_CODE_ASK_PERMISSIONS:
                takephoto();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


    // Get the results:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch( requestCode ){
            case REQUEST_IMAGE_CAPTURE:
                if( resultCode == RESULT_OK ){
                    Intent intent = new Intent(this, PreviewImage.class);
                    startActivity(intent);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }




    private void makeToast(String status){
        switch( status ){
            case "success":
                Toast.makeText(this, getResources().getString(R.string.result_success), Toast.LENGTH_LONG).show();
                break;
            case "fail":
                Toast.makeText(this, getResources().getString(R.string.result_fail), Toast.LENGTH_LONG).show();
                break;
        }
    }

}
