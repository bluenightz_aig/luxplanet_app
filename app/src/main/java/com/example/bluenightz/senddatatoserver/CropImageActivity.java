package com.example.bluenightz.senddatatoserver;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.bluenightz.senddatatoserver.manager.CameraManager;
import com.example.bluenightz.senddatatoserver.manager.FileUtils;
import com.theartofdev.edmodo.cropper.CropImageView;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.http.Path;

public class CropImageActivity extends AppCompatActivity {

    @BindView(R.id.cropImageView)
    CropImageView cropImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        ButterKnife.bind(this);

        Bitmap bitmap = BitmapFactory.decodeFile( CameraManager.getInstance().getPhotoPath() );
        cropImageView.setImageBitmap( bitmap );
    }

    public void cancelCropClick(View view) {
        Intent intent = new Intent(this, PreviewImage.class );
        startActivity( intent );
    }

    public void confirmCropClick(View view) {


//        Intent intent = new Intent(this, PreviewImage.class );
        Bitmap bitmap = cropImageView.getCroppedImage();

        File fileOriginal = new File( CameraManager.getInstance().getPhotoPath() );
        String fileName = CameraManager.getInstance().getPrefix();
        String ext = FileUtils.getExtension( fileOriginal.getName() );


        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File file = new File( storageDir + "/" + fileName + "_crop" + ext );



        try {


            if( !file.exists() ){
                try {
                    file.createNewFile();
                    CameraManager.getInstance().setmCropPhotoPath( file.getPath() );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            FileOutputStream fileOutputStream = new FileOutputStream( file );
            try {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fileOutputStream );
                setResult( RESULT_OK );
                CameraManager.getInstance().setImagePathForUpload( CameraManager.getInstance().getmCropPhotoPath() );
                saveImage( file );
                finish();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        setResult( RESULT_CANCELED );
        finish();
//        startActivity( intent );
    }



    public void saveImage(File imageFile) throws IOException {

    }

}
