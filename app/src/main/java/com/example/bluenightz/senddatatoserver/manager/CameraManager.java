package com.example.bluenightz.senddatatoserver.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class CameraManager {

    private static CameraManager instance;
    private String mCurrentPhotoPath;
    private String mCropPhotoPath;
    private String imagePathForUpload;
    private String prefix;
    private String suffix; 

    public static CameraManager getInstance() {
        if (instance == null)
            instance = new CameraManager();
        return instance;
    }

    private Context mContext;

    private CameraManager() {
        mContext = Contextor.getInstance().getContext();
    }




    public void dispatchTakePictureIntent(Activity activity, int RequestCode) {
        if( mContext == null )
            mContext = activity.getBaseContext();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile( activity );
                } catch ( NullPointerException ex ){
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(activity,
                            "com.example.bluenightz.senddatatoserver.fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    activity.startActivityForResult(takePictureIntent, RequestCode);
                }
            }
        }
    }



    private File createImageFile(Activity activity) {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp;
        prefix = imageFileName;
        suffix = ".jpg";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = new File( storageDir.getPath() + "/" + prefix + suffix);
        try{
            if( !image.exists() ){
                image.createNewFile();
            }

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = image.getAbsolutePath();
        }catch( Exception e ){
            e.printStackTrace();
        }
        return image;
    }

    public String getPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void setPhotoPath(String str){
        mCurrentPhotoPath = str;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getmCropPhotoPath() {
        return mCropPhotoPath;
    }

    public void setmCropPhotoPath(String mCropPhotoPath) {
        this.mCropPhotoPath = mCropPhotoPath;
    }

    public String getImagePathForUpload() {
        return imagePathForUpload;
    }

    public void setImagePathForUpload(String imagePathForUpload) {
        this.imagePathForUpload = imagePathForUpload;
    }
}
