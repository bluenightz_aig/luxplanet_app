package com.example.bluenightz.senddatatoserver.manager;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * Created by chattaponuthum on 15/1/2018 AD.
 */

public class FileUtils {

    public static String getFileNameOnly( String path ){
        File file = new File( path );
        return file.getName().replaceFirst("[.][^.]+$", "");
    }

    public static String getExtension( String fileName ){
        return fileName.substring(fileName.lastIndexOf("."));
    }

    public static String getDir( Context c ){
        return c.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();
    }

}
