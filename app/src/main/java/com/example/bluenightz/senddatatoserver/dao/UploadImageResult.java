package com.example.bluenightz.senddatatoserver.dao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bluenightz on 5/18/2017 AD.
 */

public class UploadImageResult {

    @SerializedName("status")
    private boolean status;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lastname")
    private String lastname;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
