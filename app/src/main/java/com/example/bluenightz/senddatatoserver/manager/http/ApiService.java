package com.example.bluenightz.senddatatoserver.manager.http;



import com.example.bluenightz.senddatatoserver.dao.UploadImageResult;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by bluenightz on 1/16/2017 AD.
 */

public interface ApiService {

    @Multipart
    @POST("saleapp/api/uploadimage")
    Call<UploadImageResult> uploadImage(@Part("firstname") String firstname, @Part("lastname") String lastname, @Part MultipartBody.Part file);


//    @GET("api/barcode/index/{id}/{numbarcode}/{problemMessage}")
//    Call<UploadImageResult> scanbarcode(
//            @Path("id") String id,
//            @Path("numbarcode") String numbarcode,
//            @Path("problemMessage") String problemMessage
//    );



//    @FormUrlEncoded
//    @POST("api/insertsalevolumn")
//    Call<ReqResult> insertsalevolumn(
//            @Header("Authorization") String credentials,
//            @Field("branch_id") String branch_id,
//            @Field("amount") String amount,
//            @Field("upload_by") String upload_by,
//            @Field("upload_date") String upload_date
//    );
//
//
//    @FormUrlEncoded
//    @POST("api/updatesalevolumn")
//    Call<ReqResult> updatesalevolumn(
//            @Header("Authorization") String credentials,
//            @Field("id") int id,
//            @Field("amount") String amount,
//            @Field("user_id") String user_id
//    );
//
//
//
//    @GET("api/getsalevolumn/month/{m}/year/{y}/userid/{u}")
//    Call<SaleVolumnResult> getsalevolumn(
//            @Header("Authorization") String credentials,
//            @Path("m") int month,
//            @Path("y") int year,
//            @Path("u") int userid
//    );



//    @FormUrlEncoded
//    @POST("api_savecard")
//    Call<HashResult> savecard_android(
//            @Field("input_imgpath") String input_imgpath,
//            @Field("input_qrpath") String input_qrpath,
//            @Field("input_youtube") String input_youtube,
//            @Field("input_titlework") String input_titlework,
//            @Field("input_message") String input_message
//    );
//
//    @FormUrlEncoded
//    @POST("api_getqrcodefromhash")
//    Call<QrResult> getqrcodefromhash(
//            @Field("hash") String hash,
//            @Field("input_qrpath") String input_qrpath
//    );
//
//    @FormUrlEncoded
//    @POST("api_sendmail")
//    Call<EmailResult> sendmail(
//            @Field("input_email") String input_email,
//            @Field("qrpath") String qrpath
//    );


}
