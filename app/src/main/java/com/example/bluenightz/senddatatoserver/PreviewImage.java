package com.example.bluenightz.senddatatoserver;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.bluenightz.senddatatoserver.dao.UploadImageResult;
import com.example.bluenightz.senddatatoserver.manager.CameraManager;
import com.example.bluenightz.senddatatoserver.manager.FileUtils;
import com.example.bluenightz.senddatatoserver.manager.HttpManager;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviewImage extends AppCompatActivity {

    public static final int CROP_REQUEST_CODE = 100;

    @BindView(R.id.btn_upload_image)
    Button btn_upload_image;

    @BindView(R.id.tv_firstname)
    EditText tv_firtname;

    @BindView(R.id.tv_lastname)
    EditText tv_lastname;

    @BindView(R.id.loading)
    RelativeLayout loading;

    @BindView(R.id.imageView)
    ImageView previewImage;

    @BindView(R.id.btn_take_new_photo)
    Button btn_take_new_photo;

    File compressedImageFile;
    File newCompressFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);
        ButterKnife.bind(this);
        init();
        try{
            showImage( newCompressFile.getAbsolutePath() );
        }catch(Exception e){
            showImage( CameraManager.getInstance().getPhotoPath() );
            e.printStackTrace();
        }
    }

    private void init() {


        compressImage();
        btn_upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload_Image();
            }
        });

    }

    public void btnEditImageFunc( View view ){
        Intent intent = new Intent( this, CropImageActivity.class );
        startActivityForResult( intent, CROP_REQUEST_CODE );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( requestCode == CROP_REQUEST_CODE ){
            if( resultCode == RESULT_OK ){
                Log.d("onActivityResult", "RESULT_OK");
                if( !CameraManager.getInstance().getmCropPhotoPath().equals(null) )
                    showImage( CameraManager.getInstance().getmCropPhotoPath() );
            }else{
                showImage( CameraManager.getInstance().getPhotoPath() );
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void compressImage() {
        try {
            File imageFile = new File( CameraManager.getInstance().getPhotoPath() );
            // Use compress library and get compress file
            compressedImageFile = new Compressor(this).compressToFile(imageFile);
            copyAndDelete(imageFile);

        }catch( IOException e ){

            e.printStackTrace();

        }
    }

    public void copyAndDelete(File imageFile) throws IOException {

        // Read file
        int length = (int) compressedImageFile.length();

        byte[] bytes = new byte[length];

        FileInputStream in = new FileInputStream(compressedImageFile);
        try {

            in.read(bytes);

        } finally {

            in.close();

        }


        // Write file
        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File originalFile = new File( CameraManager.getInstance().getPhotoPath() );
        String fileName = CameraManager.getInstance().getPrefix();
        String ext = FileUtils.getExtension( originalFile.getName() );

        if (storageDir != null) {
            newCompressFile = new File( storageDir.getPath() + "/" + fileName + "_compress" + ext );
        }


        try{

            if( !newCompressFile.exists() ){
                newCompressFile.createNewFile();
            }

            FileOutputStream fileOutputStream = new FileOutputStream( newCompressFile );
            fileOutputStream.write( bytes );
            fileOutputStream.close();

            CameraManager.getInstance().setPhotoPath( newCompressFile.getAbsolutePath() );
            CameraManager.getInstance().setImagePathForUpload( newCompressFile.getAbsolutePath() );
            CameraManager.getInstance().setPhotoPath( newCompressFile.getAbsolutePath() );
            imageFile.delete();

        }catch( Exception e ){

            e.printStackTrace();

        }
    }

    private void showloading(){
        loading.setVisibility(View.VISIBLE);
    }

    private void hideloading(){
        loading.setVisibility(View.GONE);
    }

    private void upload_Image() {

        // pass it like this
        // String filename = CameraManager.getInstance().getPhotoPath();
        File file = new File( CameraManager.getInstance().getImagePathForUpload() );
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        String firstname = tv_firtname.getText().toString();
        String lastname = tv_lastname.getText().toString();

        showloading();



        Call<UploadImageResult> call = HttpManager.getInstance().getService().uploadImage( firstname, lastname, body );
        call.enqueue(new Callback<UploadImageResult>() {
            @Override
            public void onResponse(Call<UploadImageResult> call, Response<UploadImageResult> response) {
                Log.d("UploadImageResult", "Success");
                Toast.makeText(getBaseContext(), R.string.send_data_completed, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity( intent );
                hideloading();
            }

            @Override
            public void onFailure(Call<UploadImageResult> call, Throwable t) {
                Log.d("UploadImageResult", t.getMessage());
                Toast.makeText(getBaseContext(), R.string.cannot_connect_server, Toast.LENGTH_LONG).show();
                hideloading();
            }
        });

    }

    private void showImage( String path ){
        File imgFile = new File( path );

        if(imgFile.exists()){

            try {
                CameraManager.getInstance().setImagePathForUpload( imgFile.getPath() );

                previewImage = (ImageView) findViewById(R.id.imageView);
                ExifInterface exif = new ExifInterface(path);

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                }
                else if (orientation == 3) {
                    matrix.postRotate(180);
                }
                else if (orientation == 8) {
                    matrix.postRotate(270);
                }

                myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true); // rotating bitmap

                previewImage.setImageBitmap(myBitmap);

            }catch( IOException e ){



            }

        }
    }

    public void btnTakeNewPhoto(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
